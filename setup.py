from setuptools import setup, find_packages

setup(
    name='mypackage',
    # if you name it samplepkg it will install a pakcage name mypackage. So to be consistent use mypackage as name
    version='0.0.0',
    packages=find_packages(),
)
