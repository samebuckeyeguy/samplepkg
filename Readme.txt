Very Minimal Structure for Sample Python Package - multiple packages/modules under one namespace

import mypackage.run as r1
import mypackage.module1.run as r2
import mypackage.module2.run as r3

r1.main() >>> From MyPackage Folder

r2.main() >>> From module 1

r3.main() >>> From module 2
